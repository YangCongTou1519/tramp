package com.tramp.controller;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;

import java.util.UUID;

import com.tramp.utils.JSONUtils;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import com.tramp.BaseTest;
import com.tramp.basic.entity.Demo;
import com.tramp.basic.service.DemoService;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

/**
 * demo测试类
 * @author chenjm1
 * @since 2017/12/1
 */
public class DemoControllerTest extends BaseTest {

	@InjectMocks
	@Autowired
	private DemoController controller;

	@Mock
	private DemoService demoService;

	private MockMvc mockMvc;

    @Rule
    public ExpectedException exception = ExpectedException.none();

	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
        this.mockMvc = MockMvcBuilders.standaloneSetup(controller).build();
    }

	/**
     * get测试
     */
    @Test
    public void getTest() throws Exception{
        //mock数据
        String id =UUID.randomUUID().toString();
        Demo demo = new Demo();
        demo.setId(id);
        demo.setName("demo");
        demo.setStatus(1);
        when(demoService.get(id)).thenReturn(demo);
        //请求
        mockMvc.perform(MockMvcRequestBuilders.post("/demo/get").param("id",id))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.code").value(200))
                .andExpect(MockMvcResultMatchers.jsonPath("$.data.name").value(demo.getName()));
    }

    /**
     * insert测试
     */
    @Test
    public void insertTest() throws Exception{
        //mock数据
        String id =UUID.randomUUID().toString();
        Demo demo = new Demo();
        demo.setId(id);
        demo.setName("demo");
        demo.setStatus(1);
        when(demoService.insert(any(Demo.class))).thenReturn(demo);
        //请求
        mockMvc.perform(MockMvcRequestBuilders.post("/demo/insert").contentType(MediaType.APPLICATION_JSON)
                .content(JSONUtils.toJson(demo)).accept(MediaType.ALL))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.code").value(200))
                .andExpect(MockMvcResultMatchers.jsonPath("$.data.name").value(demo.getName()));
    }

    /**
     * delete测试
     */
    @Test
    public void deleteTest() throws Exception{
        //mock数据
        String id =UUID.randomUUID().toString();
        doNothing().when(demoService).delete(id);
        //请求
        mockMvc.perform(MockMvcRequestBuilders.post("/demo/delete").param("id",id))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.code").value(200));
    }

    /**
     * update测试
     */
    @Test
    public void updateTest() throws Exception{
        //mock数据
        String id =UUID.randomUUID().toString();
        Demo demo = new Demo();
        demo.setId(id);
        demo.setName("demo");
        demo.setStatus(1);
        when(demoService.update(any(Demo.class))).thenReturn(demo);
        //请求
        mockMvc.perform(MockMvcRequestBuilders.post("/demo/update").contentType(MediaType.APPLICATION_JSON)
                .content(JSONUtils.toJson(demo)).accept(MediaType.ALL))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.code").value(200))
                .andExpect(MockMvcResultMatchers.jsonPath("$.data.name").value(demo.getName()));
    }

    @Test
    public void genericExceptionTest() throws Exception{
        exception.expectMessage("genericException测试");
        //请求
        mockMvc.perform(MockMvcRequestBuilders.post("/demo/genericException"));
    }
}
