/**
 * 用户管理js
 */

$(function() {
	initialPage();
	getGrid();
});

function initialPage() {
	$(window).resize(function() {
		$('#dataGrid').bootstrapTable('resetView', {
			height : $(window).height() - 54
		});
	});
}

function getGrid() {
	$('#dataGrid').bootstrapTableEx({
		url : '../../admin/query',
		height : $(window).height() - 54,
		queryParams : function(params) {
            return {
                pageSize: params.pageSize,
                currentPage:params.pageNumber
            };
		},
		columns : [ {
            radio:true
        }, {
            field: 'username',
            align:'center',
            title: '用户名'
        }, {
            field: 'name',
            align:'center',
            title: '姓名'
        }, {
			field : "status",
			title : "状态",
			width : "60px",
			formatter : function(value, row, index) {
				if (value == '1') {
					return '<span class="label label-success">正常</span>';
				}else{
                    return '<span class="label label-danger">禁用</span>';
                }
			}
		}, {
			field : "gmtCreate",
			title : "创建时间",
			width : "200px"
		} ]
	})
}

var vm = new Vue({
	el : '#dpLTE',
	data : {
		keyword : null
	},
	methods : {
		load : function() {
			$('#dataGrid').bootstrapTable('refresh');
		},
		save : function() {
			dialogOpen({
				title : '新增用户',
				url : host+'admin/add?_' + $.now(),
				width : '600px',
				height : '350px',
				scroll : true,
				yes : function(iframeId) {
					top.frames[iframeId].vm.acceptClick();
				},
			});
		},
		edit : function() {
			var ck = $('#dataGrid').bootstrapTable('getSelections');
			if (checkedRow(ck)) {
				dialogOpen({
					title : '编辑用户',
					url : host+'admin/add?_' + $.now(),
					width : '600px',
					height : '350px',
					scroll : true,
					success : function(iframeId) {
						top.frames[iframeId].vm.admin.id = ck[0].id;
						top.frames[iframeId].vm.setForm();
					},
					yes : function(iframeId) {
						top.frames[iframeId].vm.acceptClick();
					},
				});
			}
		},
		remove : function() {
			var ck = $('#dataGrid').bootstrapTable('getSelections'), ids = [];
			if (checkedArray(ck)) {
				$.each(ck, function(idx, item) {
					ids[idx] = item.userId;
				});
				$.deleteForm({
					url : host+'admin/remove?_' + $.now(),
                    param: {"id":ck[0].id},
					success : function(data) {
						vm.load();
					}
				});
			}
		},
		disable : function() {
			var ck = $('#dataGrid').bootstrapTable('getSelections');
			if (checkedArray(ck)) {
				$.ConfirmForm({
					msg : '您是否要禁用所选账户吗？',
					url : host+'admin/disable?_' + $.now(),
                    param: {"id":ck[0].id},
					success : function(data) {
						vm.load();
					}
				});
			}
		},
		enable : function() {
			var ck = $('#dataGrid').bootstrapTable('getSelections');
			if (checkedArray(ck)) {
				$.ConfirmForm({
					msg : '您是否要启用所选账户吗？',
					url : host+'admin/enable?_' + $.now(),
                    param: {"id":ck[0].id},
					success : function(data) {
						vm.load();
					}
				});
			}
		},
		reset : function() {
			var ck = $('#dataGrid').bootstrapTable('getSelections');
			if (checkedRow(ck)) {
				dialogOpen({
					title : '重置密码',
					url : host+'admin/reset?_' + $.now(),
					width : '400px',
					height : '220px',
					success : function(iframeId) {
						top.frames[iframeId].vm.admin.id = ck[0].id;
					},
					yes : function(iframeId) {
						top.frames[iframeId].vm.acceptClick();
					},
				});
			}
		}
	}
})