/**
 * 新增-用户管理js
 */
var vm = new Vue({
	el:'#dpLTE',
	data: {
		roleList:{},
		admin:{
			status: true,
			roleIdList:[]
		}
	},
	methods : {
        setForm: function() {
            $.ajax({
                url : host+'admin/info?_' + $.now(),
                data : {"id":vm.admin.id},
                success : function(data) {
                    if (data.code != '200') {
                        dialogAlert(data.msg, 'error');
                    } else if (data.code == '200') {
                        vm.admin = data.data;
                    }
                },
                error : function(XMLHttpRequest, textStatus, errorThrown) {
                    dialogLoading(false);
                    if(XMLHttpRequest.responseJSON.code == 401){
                        toUrl('login.html');
                    } else if(textStatus=="error"){
                        dialogMsg("请求超时，请稍候重试...", "error");
                    } else {
                        dialogMsg(errorThrown, 'error');
                    }
                }
            });

        },
		getRoleList: function(){
			$.get(host+'role/select?_' + $.now(), function(r){
				vm.roleList = r.data;
			});
		},
		acceptClick: function() {
			if (!$('#form').Validform()) {
		        return false;
		    }
		    $.SaveForm({
		    	url: host+'admin/save?_' + $.now(),
		    	param: vm.admin,
		    	success: function(data) {
		    		$.currentIframe().vm.load();
		    	}
		    });
		}
	},
	created : function() {
		this.getRoleList();
	}
})
