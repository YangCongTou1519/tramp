/**
 * 新增-菜单管理js
 */
var vm = new Vue({
	el:'#dpLTE',
	data: {
		menu:{
			parentName:null,
			parentId:0,
			type:1,
			ordering:0,
			icon: null
		}
	},
	methods : {
		selectIcon: function() {
			dialogOpen({
				id: 'iconSelect',
				title: '选取图标',
		        url: host+'menu/icon.html?_' + $.now(),
		        scroll : true,
		        width: "1030px",
		        height: "600px",
		        btn: false
		    })
		},
		menuTree: function(){
		    dialogOpen({
				id: 'layerMenuTree',
				title: '选择菜单',
		        url: host+'menu/tree.html?_' + $.now(),
		        scroll : true,
		        width: "300px",
		        height: "450px",
		        yes : function(iframeId) {
		        	top.frames[iframeId].vm.acceptClick();
				}
		    })
		},
        setForm: function() {
            $.ajax({
                url : host+'menu/info?_' + $.now(),
                data : {"id":vm.menu.id},
                success : function(data) {
                    if (data.code != '200') {
                        dialogAlert(data.msg, 'error');
                    } else if (data.code == '200') {
                        vm.menu = data.data;
                    }
                },
                error : function(XMLHttpRequest, textStatus, errorThrown) {
                    dialogLoading(false);
                    if(XMLHttpRequest.responseJSON.code == 401){
                        toUrl('login.html');
                    } else if(textStatus=="error"){
                        dialogMsg("请求超时，请稍候重试...", "error");
                    } else {
                        dialogMsg(errorThrown, 'error');
                    }
                }
            });

        },
		acceptClick: function() {
			if (!$('#form').Validform()) {
		        return false;
		    }
		    $.SaveForm({
		    	url: host+'menu/save?_' + $.now(),
		    	param: vm.menu,
		    	success: function(data) {
		    		$.currentIframe().vm.load();
		    	}
		    });
		}
	}
})
