/**
 * 字典类型管理js
 */

$(function () {
	initialPage();
	getGrid();
});

function initialPage() {
	$(window).resize(function() {
		$('#dataGrid').bootstrapTable('resetView', {height: $(window).height()-54});
	});
}

function getGrid() {
	$('#dataGrid').bootstrapTableEx({
        url: host+'dictType/query?_' + $.now(),
		height: $(window).height()-54,
		queryParams: function(params){
            return {
                name:vm.keyword,
                pageSize: params.pageSize,
                currentPage:params.pageNumber
			}
		},
		columns: [{
            radio:true
		}, {
			field : "name",
			title : "名称",
			width : "200px"
		}, {
            field : "value",
            title : "值",
            width : "200px"
        }, {
            field : "remark",
            title : "备注",
            width : "200px"
        }, {
            field : "value",
            title : "操作",
            width : "200px",
            formatter : function(value, row, index) {
            	var html = "";
            	if(hasPermission('dictType_edit')){
                    html+='<a class="btn btn-default" onclick="edit(\''+row.id+'\');"><i class="fa fa-pencil-square-o"></i></a>';
                }
                if(hasPermission('dictType_del')){
                    html+='&nbsp;<a class="btn btn-default" onclick="delet(\''+row.id+'\');"><i class="fa fa-trash-o"></i></a>';
                }
                if(hasPermission('dictType_detail')){
                    html+='&nbsp;<a class="btn btn-default" onclick="dataDetail(\''+row.id+'\');"><i class="fa fa-eye"></i></a>';
                }
                return html;
            }
        }]
	})
}

function dataDetail(id) {
	console.info("dataDetail:"+id);
    location.href=host+'dictData/list.html?typeId='+id;
}
function edit(id) {
    if(checkedRow(id)){
        dialogOpen({
            title: '编辑',
            url: host+'dictType/add.html?_' + $.now(),
            width: '420px',
            height: '350px',
            success: function(iframeId){
                top.frames[iframeId].vm.dictType.id = id;
                top.frames[iframeId].vm.setForm();
            },
            yes: function(iframeId){
                top.frames[iframeId].vm.acceptClick();
            }
        });
    }
}

function delet(id) {
    if(checkedArray(id)){
        $.deleteForm({
            url: host+'dictType/delete?_' + $.now(),
            param: {"id":id},
            success: function(data) {
                vm.load();
            }
        });
    }
}

var vm = new Vue({
	el:'#dpLTE',
	data: {
		keyword: null
	},
	methods : {
		load: function() {
			$('#dataGrid').bootstrapTable('refresh');
		},
		save: function() {
			dialogOpen({
				title: '新增',
                url: host+'dictType/add.html?_' + $.now(),
				width: '520px',
				height: '350px',
				yes : function(iframeId) {
					top.frames[iframeId].vm.acceptClick();
				},
			});
		}
	}
})