/**
 * 新增-角色管理js
 */
var vm = new Vue({
	el:'#dpLTE',
	data: {
        dictType: {
            status:true
        }
	},
	methods : {
        setForm: function() {
            $.ajax({
                url : host+'dictType/get?_' + $.now(),
                data : {"id":vm.dictType.id},
                success : function(data) {
                    if (data.code != '200') {
                        dialogAlert(data.msg, 'error');
                    } else if (data.code == '200') {
                        vm.dictType = data.data;
                    }
                },
                error : function(XMLHttpRequest, textStatus, errorThrown) {
                    dialogLoading(false);
                    if(XMLHttpRequest.responseJSON.code == 401){
                        toUrl('login.html');
                    } else if(textStatus=="error"){
                        dialogMsg("请求超时，请稍候重试...", "error");
                    } else {
                        dialogMsg(errorThrown, 'error');
                    }
                }
            });

        },
		acceptClick: function() {
			if (!$('#form').Validform()) {
		        return false;
		    }
		    $.SaveForm({
                url: host+'dictType/save?_' + $.now(),
		    	param: vm.dictType,
		    	success: function(data) {
		    		$.currentIframe().vm.load();
		    	}
		    });
		}
	}
})
