/**
 * 新增-角色管理js
 */
var vm = new Vue({
	el:'#dpLTE',
	data: {
        dictData: {
            status:true
        }
	},
	methods : {
        setForm: function() {
            $.ajax({
                url : host+'dictData/get?_' + $.now(),
                data : {"id":vm.dictData.id},
                success : function(data) {
                    if (data.code != '200') {
                        dialogAlert(data.msg, 'error');
                    } else if (data.code == '200') {
                        vm.dictData = data.data;
                    }
                },
                error : function(XMLHttpRequest, textStatus, errorThrown) {
                    dialogLoading(false);
                    if(XMLHttpRequest.responseJSON.code == 401){
                        toUrl('login.html');
                    } else if(textStatus=="error"){
                        dialogMsg("请求超时，请稍候重试...", "error");
                    } else {
                        dialogMsg(errorThrown, 'error');
                    }
                }
            });

        },
		acceptClick: function() {
			if (!$('#form').Validform()) {
		        return false;
		    }
		    $.SaveForm({
                url: host+'dictData/save?_' + $.now(),
		    	param: vm.dictData,
		    	success: function(data) {
		    		$.currentIframe().vm.load();
		    	}
		    });
		}
	}
})
