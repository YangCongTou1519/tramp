/**
 * 字典类型管理js
 */

$(function () {
	initialPage();
	getGrid();
});

function initialPage() {
	$(window).resize(function() {
		$('#dataGrid').bootstrapTable('resetView', {height: $(window).height()-54});
	});
}

function getGrid() {
	$('#dataGrid').bootstrapTableEx({
        url: host+'dictData/query?_' + $.now(),
		height: $(window).height()-54,
		queryParams: function(params){
            return {
            	typeId:url("typeId"),
                name:vm.keyword,
                pageSize: params.pageSize,
                currentPage:params.pageNumber
			}
		},
		columns: [{
            radio:true
		}, {
			field : "name",
			title : "名称",
			width : "200px"
		}, {
            field : "value",
            title : "值",
            width : "200px"
        }, {
            field : "remark",
            title : "备注",
            width : "200px"
        }, {
            field : "value",
            title : "操作",
            width : "200px",
            formatter : function(value, row, index) {
            	var html = "";
                html+='<a class="btn btn-default" onclick="edit(\''+row.id+'\');"><i class="fa fa-pencil-square-o"></i></a>';
                html+='&nbsp;<a class="btn btn-default" onclick="delet(\''+row.id+'\');"><i class="fa fa-trash-o"></i></a>';
                return html;
            }
        }]
	})
}

function edit(id) {
    if(checkedRow(id)){
        dialogOpen({
            title: '编辑',
            url: host+'dictData/add.html?_' + $.now(),
            width: '420px',
            height: '350px',
            success: function(iframeId){
                top.frames[iframeId].vm.dictData.id = id;
                top.frames[iframeId].vm.setForm();
            },
            yes: function(iframeId){
                top.frames[iframeId].vm.acceptClick();
            }
        });
    }
}

function delet(id) {
    if(checkedArray(id)){
        $.deleteForm({
            url: host+'dictData/delete?_' + $.now(),
            param: {"id":id},
            success: function(data) {
                vm.load();
            }
        });
    }
}

var vm = new Vue({
	el:'#dpLTE',
	data: {
		keyword: null
	},
	methods : {
		load: function() {
			$('#dataGrid').bootstrapTable('refresh');
		},
		save: function() {
			dialogOpen({
				title: '新增',
                url: host+'dictData/add.html?_' + $.now(),
				width: '520px',
				height: '350px',
				yes : function(iframeId) {
                    top.frames[iframeId].vm.dictData.typeId=url("typeId");
					top.frames[iframeId].vm.acceptClick();
				},
			});
		}
	}
})