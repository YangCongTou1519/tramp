package com.tramp.utils;

import com.tramp.basic.entity.Admin;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.session.Session;
import org.apache.shiro.subject.Subject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Shiro工具类
 */
public class ShiroUtils {

    protected static Logger logger = LoggerFactory.getLogger(ShiroUtils.class);


    public static Session getSession() {
        return SecurityUtils.getSubject().getSession();
    }

    public static Subject getSubject() {
        return SecurityUtils.getSubject();
    }

    public static void setSessionAttribute(Object key, Object value) {
        getSession().setAttribute(key, value);
    }

    public static Object getSessionAttribute(Object key) {
        return getSession().getAttribute(key);
    }

    public static boolean isLogin() {
        return SecurityUtils.getSubject().getPrincipal() != null;
    }

    public static void logout() {
        SecurityUtils.getSubject().logout();
    }

    public static String getKaptcha(String key) {
        String kaptcha = getSessionAttribute(key).toString();
        getSession().removeAttribute(key);
        return kaptcha;
    }

    public static Admin getAdmin() {
        return (Admin) SecurityUtils.getSubject().getPrincipal();
    }

    public static String getUserName() {
        try {
            Admin admin = (Admin) getSubject().getPrincipal();
            if (admin == null) {
                return null;
            }
            return admin.getUsername();
        }catch (Exception e){
            logger.error("getUserName error",e);
        }
        return null;
    }
}
