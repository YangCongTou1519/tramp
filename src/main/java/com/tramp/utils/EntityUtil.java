package com.tramp.utils;

import java.lang.reflect.Method;
import java.text.SimpleDateFormat;

/**
 * Created by chen on 2017/10/22.
 */
public class EntityUtil {
    /**
     * 根据属性名获取属性值
     * */
    public static Object getFieldValueByName(String fieldName, Object o) {
        try {
            String firstLetter = fieldName.substring(0, 1).toUpperCase();
            String getter = "get" + firstLetter + fieldName.substring(1);
            Method method = o.getClass().getMethod(getter, new Class[] {});
            Object value = method.invoke(o, new Object[] {});
            if(fieldName.contains("time")){
                SimpleDateFormat dateFormat=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                value= dateFormat.format(value);
            }
            return value;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
}
