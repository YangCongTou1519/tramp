package com.tramp.controller;

import com.tramp.basic.entity.DictData;
import com.tramp.basic.entity.DictType;
import com.tramp.basic.service.DictDataService;
import com.tramp.frame.server.base.BaseController;
import com.tramp.frame.server.base.ResultData;
import com.tramp.frame.server.exception.GenericException;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

/**
 * Created by chen on 2017/12/10.
 */
@Controller
@RequestMapping("/dictData/")
public class DictDataController extends BaseController {

    @Autowired
    private DictDataService dictDataService;

    /**
     * 列表页
     *
     * @return
     */
    @RequestMapping(value = "list")
    public String list() {
        return "dictData/list";
    }

    /**
     * 列表数据
     *
     * @return
     */
    @RequestMapping("/query")
    @ResponseBody
    public ResultData query(String typeId,String name) {
        this.setPageSqlId("DictDataBaseDao.listByEntity");
        List<DictData> dictDataList = dictDataService.query(typeId,name);
        return successPage(dictDataList);
    }

    /**
     * 添加页
     *
     * @return
     */
    @RequestMapping(value = "add")
    public String add() {

        return "dictData/add";
    }

    /**
     * 保存
     *
     * @return
     */
    @RequestMapping("/save")
    @ResponseBody
    public ResultData save(@RequestBody DictData dictData) {
        dictDataService.save(dictData);
        return success(dictData);
    }

    /**
     * 根据id查询详情
     *
     * @param id
     * @return
     */
    @RequestMapping("get")
    @ResponseBody
    public ResultData get(String id) {
        if (StringUtils.isBlank(id)) {
            throw new GenericException("参数错误");
        }
        DictData dictData = dictDataService.get(id);
        return success(dictData);
    }

    /**
     * 删除
     *
     * @return
     */
    @RequestMapping("/delete")
    @ResponseBody
    public ResultData delete(String id) {
        if (StringUtils.isBlank(id)) {
            throw new GenericException("参数错误");
        }
        dictDataService.delete(id);
        return success();
    }
}
