package com.tramp.controller;

import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.google.common.collect.Lists;
import com.tramp.basic.vo.MenuVO;
import com.tramp.frame.server.base.BaseController;
import com.tramp.frame.server.base.ResultData;

/**
 * Created by chen on 2017/10/22.
 */
@Controller
@RequestMapping("/index/")
public class IndexController extends BaseController {

	/**
	 * 首页
	 * @return
	 */
	@RequestMapping("index")
	public String index() {
		return "index_tab";
	}
	/**
	 * 首页
	 * @return
	 */
	@RequestMapping("index_tab")
	public String index_tab() {
		return "index_tab";
	}

	@RequestMapping("main")
	public String main(Model model) {

		return "main";
	}

	/**
	 * 获取菜单
	 * @param session
	 * @return
	 */
	@ResponseBody
	@RequestMapping("getMenues")
	public ResultData getMenues(HttpSession session) {
		List<MenuVO> menuVOList = (List<MenuVO>) session.getAttribute("menus");
		return success(menuVOList);
	}

	/**
	 * 获取按钮码
	 * @param session
	 * @return
	 */
	@ResponseBody
	@RequestMapping("getBtnList")
	public ResultData getBtnList(HttpSession session) {
		List<String> btnCodeList = Lists.newArrayList();
		List<MenuVO> menuVOList = (List<MenuVO>) session.getAttribute("menus");
		for (MenuVO menuVO : menuVOList) {
			List<MenuVO> menuList = menuVO.getMenuList();
			for (MenuVO menu : menuList) {
				List<MenuVO> btnList = menu.getMenuList();
				for (MenuVO btn : btnList) {
					btnCodeList.add(btn.getBtnCode());
				}
			}
		}
		return success(btnCodeList);
	}
}
