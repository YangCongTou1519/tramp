package com.tramp.controller;

import com.tramp.basic.entity.Admin;
import com.tramp.basic.enums.AdminStatusEnum;
import com.tramp.basic.service.AdminRoleService;
import com.tramp.basic.service.AdminService;
import com.tramp.basic.vo.AdminVO;
import com.tramp.frame.server.base.BaseController;
import com.tramp.frame.server.base.ResultData;
import com.tramp.frame.server.base.field.AdminField;
import com.tramp.frame.server.exception.GenericException;
import com.tramp.utils.MD5Utils;
import com.tramp.utils.ShiroUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

/**
 * 管理人员控制层
 *
 * @author liulanghan
 * @since 2017-11-04 17:29:32
 */
@Controller
@RequestMapping("/admin/")
public class AdminController extends BaseController {

    @Autowired
    private AdminService adminService;
    @Autowired
    private AdminRoleService adminRoleService;

    /**
     * 列表页
     *
     * @return
     */
    @RequestMapping(value = "list")
    public String list() {
        return "admin/list";
    }

    /**
     * 列表数据
     *
     * @return
     */
    @RequestMapping("/query")
    @ResponseBody
    public ResultData query() {
        this.setPageSqlId("AdminDao.getList");
        List<Admin> adminList = adminService.getList();
        return successPage(adminList);
    }

    /**
     * 添加页
     *
     * @return
     */
    @RequestMapping(value = "add")
    public String add() {

        return "admin/add";
    }

    /**
     * 保存
     *
     * @return
     */
    @RequestMapping("/save")
    @ResponseBody
    public ResultData save(@RequestBody AdminVO adminVO) {
        adminService.saveAdmin(adminVO);
        return success(adminVO);
    }

    /**
     * 删除
     *
     * @return
     */
    @RequestMapping("/remove")
    @ResponseBody
    public ResultData remove(String id) {
        if (StringUtils.isBlank(id)) {
            throw new GenericException("参数错误");
        }
        adminService.deleteAdmin(id);
        return success();
    }

    /**
     * 根据id查询详情
     *
     * @param id
     * @return
     */
    @RequestMapping("info")
    @ResponseBody
    public ResultData info(String id) {
        if (StringUtils.isBlank(id)) {
            throw new GenericException("参数错误");
        }
        AdminVO adminVO = new AdminVO();
        Admin admin = adminService.get(id);
        BeanUtils.copyProperties(admin, adminVO);

        List<String> roleIdList = adminRoleService.getRoleIdList(admin.getId());
        adminVO.setRoleIdList(roleIdList);
        return success(adminVO);
    }

    /**
     * 获取登录信息
     *
     * @return
     */
    @RequestMapping("loginInfo")
    @ResponseBody
    public ResultData loginInfo() {
        return success(ShiroUtils.getAdmin());
    }


    /**
     * 禁用
     *
     * @return
     */
    @RequestMapping("/disable")
    @ResponseBody
    public ResultData disable(String id) {
        if (StringUtils.isBlank(id)) {
            throw new GenericException("参数错误");
        }
        Admin admin = adminService.get(id);
        if (AdminStatusEnum.STATUS_UNABLE.getCode().equals(admin.getStatus())) {
            throw new GenericException("已禁用");
        }
        admin.setStatus(AdminStatusEnum.STATUS_UNABLE.getCode());
        adminService.updateField(admin, AdminField.update().status());
        return success();
    }

    /**
     * 启用
     *
     * @return
     */
    @RequestMapping("/enable")
    @ResponseBody
    public ResultData enable(String id) {
        if (StringUtils.isBlank(id)) {
            throw new GenericException("参数错误");
        }
        Admin admin = adminService.get(id);
        if (AdminStatusEnum.STATUS_OK.getCode().equals(admin.getStatus())) {
            throw new GenericException("已启用");
        }
        admin.setStatus(AdminStatusEnum.STATUS_OK.getCode());
        adminService.updateField(admin, AdminField.update().status());
        return success();
    }

    /**
     * 重置密码页面
     *
     * @return
     */
    @RequestMapping(value = "reset")
    public String reset() {
        return "admin/reset";
    }

    /**
     * 重置密码
     *
     * @return
     */
    @RequestMapping("/resetSave")
    @ResponseBody
    public ResultData resetSave(Admin admin) {
        if (StringUtils.isBlank(admin.getId()) && StringUtils.isBlank(admin.getPassword())) {
            throw new GenericException("参数错误");
        }
        admin.setPassword(MD5Utils.encrypt(admin.getPassword()));
        adminService.updateField(admin, AdminField.update().password());
        return success();
    }

    /**
     * 修改密码
     *
     * @return
     */
    @RequestMapping("/updatePswd")
    @ResponseBody
    public ResultData updatePswd(String pswd, String newPswd) {
        if (StringUtils.isBlank(pswd) || StringUtils.isBlank(newPswd)) {
            throw new GenericException("参数错误");
        }
        if (pswd.equals(newPswd)) {
            throw new GenericException("新密码不能和原密码相同");
        }
        Admin admin = adminService.login(ShiroUtils.getUserName(), MD5Utils.encrypt(pswd));
        if (admin == null) {
            throw new GenericException("原密码错误");
        }
        admin.setPassword(MD5Utils.encrypt(newPswd));
        adminService.updateField(admin, AdminField.update().password());
        return success();
    }

}
