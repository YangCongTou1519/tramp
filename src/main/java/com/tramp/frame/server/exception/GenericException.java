package com.tramp.frame.server.exception;

/**
 * 业务通用异常
 * Created by chen on 2017/10/21.
 */
public class GenericException extends RuntimeException{
    private static final long serialVersionUID = -9079010183705787652L;

    public GenericException() {
    }
    public GenericException(String message) {
        super(message);
    }
}
