package com.tramp.frame.server.base.field;

/**
* 字典数据表字段
* @author mbg
* @since 2017-12-10 12:45:39
*/
public final class DictDataField extends BaseField {

    private DictDataField() {
        super();
    }

    public static DictDataField update() {
        return new DictDataField();
    }

    public DictDataField typeId() {
        this.addField("typeId");
        return this;
    }

    public DictDataField value() {
        this.addField("value");
        return this;
    }

    public DictDataField name() {
        this.addField("name");
        return this;
    }

    public DictDataField ext() {
        this.addField("ext");
        return this;
    }

    public DictDataField status() {
        this.addField("status");
        return this;
    }

    public DictDataField ordering() {
        this.addField("ordering");
        return this;
    }

    public DictDataField remark() {
        this.addField("remark");
        return this;
    }

    public DictDataField updateTime() {
        this.addField("updateTime");
        return this;
    }

    public DictDataField updateUser() {
        this.addField("updateUser");
        return this;
    }

}
