package com.tramp.frame.server.base.dao;

import com.tramp.frame.server.base.dao.BaseDao;

/**
* 角色菜单数据层
* @author liulanghan
* @since 2017-11-24 11:26:06
*/
public interface RoleMenuBaseDao extends BaseDao {

}
