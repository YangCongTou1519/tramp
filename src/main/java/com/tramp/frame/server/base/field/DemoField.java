package com.tramp.frame.server.base.field;

/**
* 表字段
* @author mbg
* @since 2017-12-01 10:53:03
*/
public final class DemoField extends BaseField {

    private DemoField() {
        super();
    }

    public static DemoField update() {
        return new DemoField();
    }

    public DemoField id() {
        this.addField("id");
        return this;
    }

    public DemoField name() {
        this.addField("name");
        return this;
    }

    public DemoField status() {
        this.addField("status");
        return this;
    }

}
