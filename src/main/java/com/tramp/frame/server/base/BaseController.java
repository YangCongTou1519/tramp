package com.tramp.frame.server.base;

import java.util.List;

import com.tramp.utils.ExcelWebUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.tramp.frame.server.pageplugin.PageInfoHolder;

/**
 * Created by chen on 2017/11/11.
 */
public class BaseController {
	protected final Logger logger = LoggerFactory.getLogger(this.getClass());

	@Autowired
	private PageInfoHolder pageInfoHolder;

	/**
	 * 返回结果
	 * @param data
	 * @return
	 */
	protected ResultData success(Object data) {
		ResultData resultData = new ResultData(CodeEnum.OK);
		resultData.setData(data);
		resultData.setTotal(1);
		return resultData;
	}

	/**
	 * 返回结果
	 * @return
	 */
	protected ResultData success() {
		ResultData resultData = new ResultData(CodeEnum.OK);
		return resultData;
	}

	/**
	 * 分页结果，带total
	 * @param listData
	 * @return
	 */
	protected ResultData successPage(List listData) {
		if (null != listData) {
			ExcelWebUtil.tryExportExcel(listData);
		}
		ResultData resultData = new ResultData(CodeEnum.OK);
		resultData.setTotal(pageInfoHolder.getPageCount());
		resultData.setData(listData);
		return resultData;
	}

	/**
	 * 指定当前请求进行分页的sql方法,默认支持count语句
	 * @param sqlId   mybatyis的sqlId
	 */
	protected void setPageSqlId(String sqlId) {
		this.setPageSqlId(sqlId,true);
	}

	protected void setPageSqlId(String sqlId, Boolean excuteCount) {
		pageInfoHolder.setSqlId(sqlId,excuteCount);
	}
}
