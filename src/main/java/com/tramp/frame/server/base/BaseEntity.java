package com.tramp.frame.server.base;

import java.io.Serializable;

public interface BaseEntity extends Serializable {
	String getId();

	void setId(String id);
}
