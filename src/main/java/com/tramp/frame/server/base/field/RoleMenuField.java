package com.tramp.frame.server.base.field;

/**
* 角色菜单表字段
* @author mbg
* @since 2017-12-05 11:23:36
*/
public final class RoleMenuField extends BaseField {

    private RoleMenuField() {
        super();
    }

    public static RoleMenuField update() {
        return new RoleMenuField();
    }

    public RoleMenuField roleId() {
        this.addField("roleId");
        return this;
    }

    public RoleMenuField menuId() {
        this.addField("menuId");
        return this;
    }

}
