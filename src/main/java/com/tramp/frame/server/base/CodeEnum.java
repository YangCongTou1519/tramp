package com.tramp.frame.server.base;

/**
 * code返回枚举类
 *
 * @author chen
 */
public enum CodeEnum {

    OK(200, "操作成功"),
    SERVER_ERROE(500, "服务器异常"),
    PARAMS_ERROE(400, "参数错误"),
    ILLEGAL_OPARE_ERROE(402, "非法操作"),
    RE_SUBMIT(405, "请勿重复提交"),
    /**
     * 用于自定义异常的code
     */
    GENERICEXCEPTION_CODE(432, "基本异常code"),
    CUSTOMER_NO_LOGIN(401, "未登录或登录失效"),;

    private Integer value;
    private String valueCn;

    private CodeEnum(Integer value, String valueCn) {
        this.value = value;
        this.valueCn = valueCn;
    }

    public Integer getValue() {
        return value;
    }

    public void setValue(Integer value) {
        this.value = value;
    }

    public String getValueCn() {
        return valueCn;
    }

    public void setValueCn(String valueCn) {
        this.valueCn = valueCn;
    }

    public static int getValue(String name) {
        if (name == null) {
            return 0;
        }
        for (CodeEnum enumObj : CodeEnum.values()) {
            if (name.equals(enumObj.getValueCn())) {
                return enumObj.getValue();
            }
        }
        return 0;
    }

    public ResultData toResult() {
        ResultData apiResultData = new ResultData();
        apiResultData.setCode(value);
        apiResultData.setMsg(valueCn);
        return apiResultData;
    }
}
