package com.tramp.frame.server.base.dao;

import com.tramp.frame.server.base.dao.BaseDao;

/**
* 菜单数据层
* @author liulanghan
* @since 2017-11-22 10:41:15
*/
public interface MenuBaseDao extends BaseDao {

}
