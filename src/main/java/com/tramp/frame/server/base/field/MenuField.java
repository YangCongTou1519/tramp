package com.tramp.frame.server.base.field;

/**
* 菜单表字段
* @author mbg
* @since 2017-12-05 11:23:36
*/
public final class MenuField extends BaseField {

    private MenuField() {
        super();
    }

    public static MenuField update() {
        return new MenuField();
    }

    public MenuField parentId() {
        this.addField("parentId");
        return this;
    }

    public MenuField parentName() {
        this.addField("parentName");
        return this;
    }

    public MenuField name() {
        this.addField("name");
        return this;
    }

    public MenuField icon() {
        this.addField("icon");
        return this;
    }

    public MenuField url() {
        this.addField("url");
        return this;
    }

    public MenuField status() {
        this.addField("status");
        return this;
    }

    public MenuField type() {
        this.addField("type");
        return this;
    }

    public MenuField btnCode() {
        this.addField("btnCode");
        return this;
    }

    public MenuField ordering() {
        this.addField("ordering");
        return this;
    }

    public MenuField updateTime() {
        this.addField("updateTime");
        return this;
    }

    public MenuField updateUser() {
        this.addField("updateUser");
        return this;
    }

}
