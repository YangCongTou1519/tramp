package com.tramp.frame.server.base;

import java.io.Serializable;

/**
 * api返回数据
 *
 * @author chen
 */
public class ResultData implements Serializable {

	/**
	 *
	 */
	private static final long serialVersionUID = -8360459540895700642L;

	private Integer code;
	private String msg;
	private Object data;
	private Integer total;

	public ResultData() {
	}

	public void setInfo(CodeEnum CodeEnum) {
		this.code = CodeEnum.getValue();
		this.msg = CodeEnum.getValueCn();
	}

	public ResultData(Integer code, String msg) {
		this.code = code;
		this.msg = msg;
	}

	public ResultData(Integer code, Object data) {
		this.code = code;
		this.data = data;
	}

	public ResultData(CodeEnum codeEnum, Object data) {
		this.code = codeEnum.getValue();
		this.msg = codeEnum.getValueCn();
		this.data = data;
	}

	public Integer getTotal() {
		return total;
	}

	public void setTotal(Integer total) {
		this.total = total;
	}

	public ResultData(CodeEnum codeEnum) {
		this.code = codeEnum.getValue();
		this.msg = codeEnum.getValueCn();
	}

	public Integer getCode() {
		return code;
	}

	public static ResultData success(Object data) {
		return new ResultData(CodeEnum.OK, data);
	}

	public static ResultData success() {
		return new ResultData(CodeEnum.OK, null);
	}

	public static ResultData error(Integer code, String msg) {
		return new ResultData(code, msg);
	}

	public void setCode(Integer code) {
		this.code = code;
	}

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}

	public Object getData() {
		return data;
	}

	public void setData(Object data) {
		this.data = data;
	}

}
