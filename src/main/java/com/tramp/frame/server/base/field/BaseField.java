package com.tramp.frame.server.base.field;

import java.util.HashMap;
import java.util.Map;

public abstract class BaseField {

	private Map<String, String> updateFieldMap = new HashMap<>();

	public Map<String, String> getUpdateFieldMap() {
		return updateFieldMap;
	}

	protected void addField(String fieldName) {
		updateFieldMap.put(fieldName, "");
	}
}
