/**
 *  
 *  CustomerLogQueryController.java 2016-12-22
 * <p/>
 * Copyright 2000-2016 by ChinanetCenter Corporation.
 * <p/>
 * All rights reserved.
 * <p/>
 * This software is the confidential and proprietary information of
 * ChinanetCenter Corporation ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with ChinanetCenter.
 */

package com.tramp.frame.server.pageplugin;

import java.io.Serializable;

/**
 * 分页信息
 * @author chenjm1
 * @since 2017/11/10
 */
public class PageInfo implements Serializable {
    private Integer total;
    private String sqlId;
    private Integer start;
    private Integer limit;
    private Boolean excuteCount;

    public String getSqlId() {
        return sqlId;
    }

    public void setSqlId(String sqlId) {
        this.sqlId = sqlId;
    }

    public Boolean getExcuteCount() {
        return excuteCount;
    }

    public void setExcuteCount(Boolean excuteCount) {
        this.excuteCount = excuteCount;
    }

    public Integer getStart() {
        return start;
    }

    public void setStart(Integer start) {
        this.start = start;
    }

    public Integer getLimit() {
        return limit;
    }

    public void setLimit(Integer limit) {
        this.limit = limit;
    }

    public Integer getTotal() {
        return total;
    }

    public void setTotal(Integer total) {
        this.total = total;
    }
}
