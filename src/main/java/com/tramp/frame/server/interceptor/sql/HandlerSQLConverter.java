package com.tramp.frame.server.interceptor.sql;

import java.sql.Connection;

import org.apache.ibatis.mapping.BoundSql;
import org.apache.ibatis.mapping.MappedStatement;

/**
 * SQL转换器接口类
 */
public interface HandlerSQLConverter {
	void convertSql(Connection connection, MappedStatement mappedStatement, BoundSql boundSql) throws Throwable;
}
