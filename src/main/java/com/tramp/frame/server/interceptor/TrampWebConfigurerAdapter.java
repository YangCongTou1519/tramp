package com.tramp.frame.server.interceptor;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

/**
 * Created by chen on 2017/10/22.
 */
@Configuration
public class TrampWebConfigurerAdapter extends WebMvcConfigurerAdapter {
	@Override
	public void addInterceptors(InterceptorRegistry registry) {
		registry.addInterceptor(new TrampInterceptor()).addPathPatterns("/**");
		super.addInterceptors(registry);
	}
}
