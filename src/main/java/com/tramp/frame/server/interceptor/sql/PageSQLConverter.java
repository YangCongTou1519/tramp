package com.tramp.frame.server.interceptor.sql;

import java.lang.reflect.Field;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.apache.ibatis.executor.ErrorContext;
import org.apache.ibatis.executor.ExecutorException;
import org.apache.ibatis.mapping.BoundSql;
import org.apache.ibatis.mapping.MappedStatement;
import org.apache.ibatis.mapping.ParameterMapping;
import org.apache.ibatis.mapping.ParameterMode;
import org.apache.ibatis.reflection.MetaObject;
import org.apache.ibatis.reflection.property.PropertyTokenizer;
import org.apache.ibatis.scripting.xmltags.ForEachSqlNode;
import org.apache.ibatis.session.Configuration;
import org.apache.ibatis.type.TypeHandler;
import org.apache.ibatis.type.TypeHandlerRegistry;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.tramp.frame.server.exception.GenericException;
import com.tramp.frame.server.pageplugin.PageInfoHolder;
import com.tramp.utils.ClassUtil;
import org.springframework.stereotype.Component;

/**
 * 分页转换 一定要放在最后一个
 */
@Component
public class PageSQLConverter implements HandlerSQLConverter {

	private static final Logger LOGGER = LoggerFactory.getLogger(PageSQLConverter.class);

	public static final String META_PARAMETERS = "metaParameters";

	private PageInfoHolder pageInfoHolder;

	@Override
	public void convertSql(Connection connection, MappedStatement mappedStatement, BoundSql boundSql) throws Throwable {
		String sql = boundSql.getSql();
		if (pageInfoHolder.isNeedPage(mappedStatement.getId())) {
			ResultSet rs = null;
			PreparedStatement countStmt = null;
			if (pageInfoHolder.excuteCount()) {
				try {
					Object parameterObject = boundSql.getParameterObject();
					String countSql = "select count(1) from (" + sql + ") myCount";
					Integer count = 0;
					countStmt = connection.prepareStatement(countSql);
					BoundSql countBS = new BoundSql(mappedStatement.getConfiguration(), countSql, boundSql.getParameterMappings(), parameterObject);
					Field metaParamsField = ClassUtil.getFieldByFieldName(boundSql, META_PARAMETERS);
					if (metaParamsField != null) {
						MetaObject mo = (MetaObject) ClassUtil.getValueByFieldName(boundSql, META_PARAMETERS);
						ClassUtil.setValueByFieldName(countBS, META_PARAMETERS, mo);
					}
					setParameters(countStmt, mappedStatement, countBS, parameterObject);
					rs = countStmt.executeQuery();
					if (rs.next()) {
						count = rs.getInt(1);
					}
					pageInfoHolder.setTotalCount(count);
				}
				catch (Exception e) {
					LOGGER.error("DO PAGE FAIL!sqlId:{}", e, mappedStatement.getId());
					throw new GenericException("分页失败");
				}
				finally {
					if (null != rs) {
						try {
							rs.close();
						}
						catch (Exception e) {
							LOGGER.error("关闭RS时报错", e);
						}

					}
					if (null != countStmt) {
						try {
							countStmt.close();
						}
						catch (Exception e) {
							LOGGER.error("关闭countStmt时报错", e);
						}
					}
				}
			}

			StringBuilder pageSql = new StringBuilder();
			pageSql.append(sql);
			pageSql.append(" limit ");
			pageSql.append(pageInfoHolder.getPageInfo().getStart() - 1);
			pageSql.append(",");
			pageSql.append(pageInfoHolder.getPageInfo().getLimit());
			ClassUtil.setValueByFieldName(boundSql, "sql", pageSql.toString());
		}
	}

	public PageInfoHolder getPageInfoHolder() {
		return pageInfoHolder;
	}

	public void setPageInfoHolder(PageInfoHolder pageInfoHolder) {
		this.pageInfoHolder = pageInfoHolder;
	}

	private void setParameters(PreparedStatement ps, MappedStatement mappedStatement, BoundSql boundSql, Object parameterObject) throws SQLException {
		ErrorContext.instance().activity("setting parameters").object(mappedStatement.getParameterMap().getId());
		List<ParameterMapping> parameterMappings = boundSql.getParameterMappings();
		if (parameterMappings != null) {
			Configuration configuration = mappedStatement.getConfiguration();
			TypeHandlerRegistry typeHandlerRegistry = configuration.getTypeHandlerRegistry();
			MetaObject metaObject = parameterObject == null ? null : configuration.newMetaObject(parameterObject);
			for (int i = 0; i < parameterMappings.size(); i++) {
				ParameterMapping parameterMapping = parameterMappings.get(i);
				if (parameterMapping.getMode() != ParameterMode.OUT) {
					Object value;
					String propertyName = parameterMapping.getProperty();
					PropertyTokenizer prop = new PropertyTokenizer(propertyName);
					if (parameterObject == null) {
						value = null;
					}
					else if (typeHandlerRegistry.hasTypeHandler(parameterObject.getClass())) {
						value = parameterObject;
					}
					else if (boundSql.hasAdditionalParameter(propertyName)) {
						value = boundSql.getAdditionalParameter(propertyName);
					}
					else if (propertyName.startsWith(ForEachSqlNode.ITEM_PREFIX) && boundSql.hasAdditionalParameter(prop.getName())) {
						value = boundSql.getAdditionalParameter(prop.getName());
						if (value != null) {
							value = configuration.newMetaObject(value).getValue(propertyName.substring(prop.getName().length()));
						}
					}
					else {
						value = metaObject == null ? null : metaObject.getValue(propertyName);
					}
					TypeHandler typeHandler = parameterMapping.getTypeHandler();
					if (typeHandler == null) {
						throw new ExecutorException(
								"There was no TypeHandler found for parameter " + propertyName + " of statement " + mappedStatement.getId());
					}
					typeHandler.setParameter(ps, i + 1, value, parameterMapping.getJdbcType());
				}
			}
		}
	}
}
