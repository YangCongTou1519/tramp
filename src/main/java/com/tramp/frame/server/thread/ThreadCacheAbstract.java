
package com.tramp.frame.server.thread;

import java.io.Serializable;

/**
 */
public abstract class ThreadCacheAbstract<T extends Serializable> implements Serializable {

	private final String cacheName;

	protected ThreadCacheAbstract(String cacheName) {
		this.cacheName = cacheName;
	}

	protected T getCache() {
		Object object = ThreadCacheStore.getCache(cacheName);
		if (null != object) {
			return (T) object;
		}
		else {
			return null;
		}
	}

	protected void setCache(T cacheObj) {
		ThreadCacheStore.setCache(cacheName, cacheObj);
	}
}
