package com.tramp.basic.dao;


import com.tramp.basic.entity.DictData;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * 字典数据数据层
 *
 * @author liulanghan
 * @since 2017-12-02 12:59:31
 */
public interface DictDataDao {

    List<DictData> query(Map<String, Object> params);

    DictData getByValue(@Param("typeId")String typeId,@Param("id") String id, @Param("value") String value);
}
