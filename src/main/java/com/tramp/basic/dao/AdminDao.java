package com.tramp.basic.dao;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.tramp.basic.entity.Admin;
import com.tramp.frame.server.base.dao.BaseDao;

/**
* 管理人员数据层
* @author liulanghan
* @since 2017-11-04 17:42:44
*/
public interface AdminDao {

	List<Admin> getList();

	Admin login(@Param("username") String username, @Param("password") String password);

    Admin getByUserName(@Param("username") String username);
}
