package com.tramp.basic.dao;


import com.tramp.basic.entity.DictType;
import org.apache.ibatis.annotations.Param;

/**
 * 字典类型数据层
 *
 * @author liulanghan
 * @since 2017-12-02 12:59:31
 */
public interface DictTypeDao {

    DictType getByValue(@Param("typeId") String typeId, @Param("value") String value);
}
