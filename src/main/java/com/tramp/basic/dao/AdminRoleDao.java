package com.tramp.basic.dao;

import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 管理员角色数据层
 *
 * @author liulanghan
 * @since 2017-11-23 18:17:31
 */
public interface AdminRoleDao {

    List<String> getRoleIdList(@Param("adminId") String adminId);
}
