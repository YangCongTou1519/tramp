package com.tramp.basic.dao;

import com.tramp.frame.server.base.dao.BaseDao;
import com.tramp.basic.entity.Role;
import org.apache.ibatis.annotations.Param;

/**
* 角色数据层
* @author liulanghan
* @since 2017-11-09 20:43:40
*/
public interface RoleDao {

    void deleteAdminRole(@Param("adminId") String adminId);
}
