package com.tramp.basic.service;

import java.util.List;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.google.common.collect.Lists;
import com.tramp.basic.dao.RoleDao;
import com.tramp.basic.entity.Role;
import com.tramp.basic.entity.RoleMenu;
import com.tramp.basic.vo.RoleVO;
import com.tramp.frame.server.base.BaseService;
import com.tramp.frame.server.base.dao.RoleBaseDao;

/**
 * 角色业务逻辑层
 *
 * @author liulanghan
 * @since 2017-11-09 20:43:40
 */
@Service
@Transactional
public class RoleService extends BaseService<Role> {

	@Autowired
	private RoleDao roleDao;
	@Autowired
	private MenuService menuService;
	@Autowired
	private RoleMenuService roleMenuService;

	public RoleService(RoleBaseDao roleDao) {
		super(roleDao);
	}

	public RoleVO getInfo(String id) {
		Role role = this.get(id);
		RoleVO roleVO = new RoleVO();
		BeanUtils.copyProperties(role, roleVO);
		//获取菜单ids
		List<String> menuIdList = menuService.listIdByRoleId(id);
		roleVO.setMenuIdList(menuIdList);
		return roleVO;
	}

	public void deleteAdminRole(String adminId) {
		roleDao.deleteAdminRole(adminId);
	}

	/**
	 * 保存权限
	 * @param roleVO
	 */
	public void menuAuthSave(RoleVO roleVO) {
		roleMenuService.deleteByRoleId(roleVO.getId());
		List<RoleMenu> roleMenus = Lists.newArrayList();
		List<String> menuIdList = roleVO.getMenuIdList();
		for (String menuId : menuIdList) {
			RoleMenu roleMenu = new RoleMenu();
			roleMenu.setMenuId(menuId);
			roleMenu.setRoleId(roleVO.getId());
			roleMenus.add(roleMenu);
		}
		roleMenuService.insert(roleMenus);
	}

	public void deleteRole(String id) {
		this.delete(id);
		//删除角色权限
		roleMenuService.deleteByRoleId(id);
	}
}
