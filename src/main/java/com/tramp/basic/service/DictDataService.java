package com.tramp.basic.service;

import com.google.common.collect.Maps;
import com.tramp.frame.server.base.dao.DictDataBaseDao;
import com.tramp.basic.dao.DictDataDao;
import com.tramp.frame.server.base.BaseService;
import com.tramp.frame.server.exception.GenericException;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.tramp.basic.entity.DictData;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;


/**
* 字典数据业务逻辑层
* @author liulanghan
* @since 2017-12-02 12:59:31
*/
@Service
@Transactional
public class DictDataService extends BaseService<DictData> {

    @Autowired
    private DictDataDao dictDataDao;

    public DictDataService(DictDataBaseDao dictDataDao) {
        super(dictDataDao);
    }

    public List<DictData> query(String typeId, String name) {
        Map<String,Object> params = Maps.newHashMap();
        params.put("typeId",typeId);
        params.put("name",name);
        return dictDataDao.query(params);
    }

    public void save(DictData dictData) {
        //判断是否存在
        DictData dictDataDb = dictDataDao.getByValue(dictData.getTypeId(),dictData.getId(),dictData.getValue());
        if (dictDataDb!=null){
            throw new GenericException("值已存在");
        }
        if(StringUtils.isBlank(dictData.getId())){
            this.insert(dictData);
        }else {
            this.update(dictData);
        }
    }
}
