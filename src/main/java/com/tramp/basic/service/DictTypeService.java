package com.tramp.basic.service;

import com.tramp.basic.dao.DictTypeDao;
import com.tramp.basic.entity.DictType;
import com.tramp.frame.server.base.BaseService;
import com.tramp.frame.server.base.dao.DictTypeBaseDao;
import com.tramp.frame.server.exception.GenericException;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


/**
 * 字典类型业务逻辑层
 *
 * @author liulanghan
 * @since 2017-12-02 12:59:31
 */
@Service
@Transactional
public class DictTypeService extends BaseService<DictType> {

    @Autowired
    private DictTypeDao dictTypeDao;

    public DictTypeService(DictTypeBaseDao dictTypeDao) {
        super(dictTypeDao);
    }

    public void save(DictType dictType) {
        //判断是否已存在
        DictType typeDb = dictTypeDao.getByValue(dictType.getId(),dictType.getValue());
        if(typeDb!=null){
            throw new GenericException("值已存在");
        }
        if (StringUtils.isBlank(dictType.getId())) {

            this.insert(dictType);
        } else {
            this.update(dictType);
        }
    }
}
