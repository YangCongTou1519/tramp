package com.tramp.basic.entity;

import com.tramp.frame.server.base.BaseEntity;
import java.util.Date;

/**
* 
* @author liulanghan
* @since 2017-11-22 14:49:57
*/
public class OperateLogData implements BaseEntity{

    private String id; //
    private String operateUser; //操作人
    private String operateType; //操作类型
    private String operateClass; //操作对象的类型
    private String operateJson; //操作对象JSON数据
    private String actionSn; //动作序列
    private Date operateTime; //操作时间

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getOperateUser() {
        return operateUser;
    }

    public void setOperateUser(String operateUser) {
        this.operateUser = operateUser;
    }

    public String getOperateType() {
        return operateType;
    }

    public void setOperateType(String operateType) {
        this.operateType = operateType;
    }

    public String getOperateClass() {
        return operateClass;
    }

    public void setOperateClass(String operateClass) {
        this.operateClass = operateClass;
    }

    public String getOperateJson() {
        return operateJson;
    }

    public void setOperateJson(String operateJson) {
        this.operateJson = operateJson;
    }

    public String getActionSn() {
        return actionSn;
    }

    public void setActionSn(String actionSn) {
        this.actionSn = actionSn;
    }

    public Date getOperateTime() {
        return operateTime;
    }

    public void setOperateTime(Date operateTime) {
        this.operateTime = operateTime;
    }


}
