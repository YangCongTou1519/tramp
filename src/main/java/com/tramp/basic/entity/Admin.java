package com.tramp.basic.entity;

import com.tramp.frame.server.base.BaseEntity;
import com.tramp.frame.server.base.Recordable;
import java.util.Date;


/**
* 管理人员
* @author mbg
* @since 2017-12-05 11:23:36
*/
public class Admin implements Recordable,  BaseEntity{

    private String id; //
    private String username; //用户名
    private String password; //密码
    private Boolean status; //状态
    private String name; //姓名
    private String mobile; //手机号
    private String remark; //备注
    private Date createTime; //创建时间
    private String createUser; //
    private Date updateTime; //
    private String updateUser; //

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String getCreateUser() {
        return createUser;
    }

    public void setCreateUser(String createUser) {
        this.createUser = createUser;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public String getUpdateUser() {
        return updateUser;
    }

    public void setUpdateUser(String updateUser) {
        this.updateUser = updateUser;
    }


}
