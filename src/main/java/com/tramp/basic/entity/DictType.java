package com.tramp.basic.entity;

import com.tramp.frame.server.base.BaseEntity;
import com.tramp.frame.server.base.Recordable;
import java.util.Date;


/**
* 字典类型
* @author mbg
* @since 2017-12-10 12:45:39
*/
public class DictType implements Recordable, BaseEntity{

    private String id; //主键
    private String value; //值
    private String name; //名称
    private Boolean status; //状体(1启用,0禁用)
    private Integer ordering; //排序
    private String remark; //
    private Date createTime; //创建时间
    private String createUser; //
    private Date updateTime; //
    private String updateUser; //

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public Integer getOrdering() {
        return ordering;
    }

    public void setOrdering(Integer ordering) {
        this.ordering = ordering;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String getCreateUser() {
        return createUser;
    }

    public void setCreateUser(String createUser) {
        this.createUser = createUser;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public String getUpdateUser() {
        return updateUser;
    }

    public void setUpdateUser(String updateUser) {
        this.updateUser = updateUser;
    }


}
