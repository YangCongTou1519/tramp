package com.tramp.basic.entity;

import com.tramp.frame.server.base.BaseEntity;


/**
* 
* @author liulanghan
* @since 2017-12-01 10:53:03
*/
public class Demo implements BaseEntity{

    private String id; //
    private String name; //
    private Integer status; //

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }


}
