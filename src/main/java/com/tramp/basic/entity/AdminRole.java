package com.tramp.basic.entity;

import com.tramp.frame.server.base.BaseEntity;




/**
* 管理员角色
* @author mbg
* @since 2017-12-05 11:23:36
*/
public class AdminRole implements  BaseEntity{

    private String id; //
    private String adminId; //
    private String roleId; //

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getAdminId() {
        return adminId;
    }

    public void setAdminId(String adminId) {
        this.adminId = adminId;
    }

    public String getRoleId() {
        return roleId;
    }

    public void setRoleId(String roleId) {
        this.roleId = roleId;
    }


}
