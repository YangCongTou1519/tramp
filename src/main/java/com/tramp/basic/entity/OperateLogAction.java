package com.tramp.basic.entity;

import com.tramp.frame.server.base.BaseEntity;
import java.util.Date;

/**
* 
* @author liulanghan
* @since 2017-11-22 14:49:57
*/
public class OperateLogAction implements BaseEntity{

    private String id; //
    private String actionSn; //动作序列
    private String operateUser; //操作人
    private String url; //动作url
    private String params; //参数
    private Date operateTime; //操作时间

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getActionSn() {
        return actionSn;
    }

    public void setActionSn(String actionSn) {
        this.actionSn = actionSn;
    }

    public String getOperateUser() {
        return operateUser;
    }

    public void setOperateUser(String operateUser) {
        this.operateUser = operateUser;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getParams() {
        return params;
    }

    public void setParams(String params) {
        this.params = params;
    }

    public Date getOperateTime() {
        return operateTime;
    }

    public void setOperateTime(Date operateTime) {
        this.operateTime = operateTime;
    }


}
