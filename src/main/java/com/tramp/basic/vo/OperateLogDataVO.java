package com.tramp.basic.vo;

import java.util.Date;

/**
 *  数据库修改日志
 */
public class OperateLogDataVO {
	private String operateUser;  //实际操作人
	private String loginUser;  //登录人，如值班账号
	private Date operateTime;
	private String actionSn;
	private String operateType;
	private String operateClass;
	private Object operateObj;

	public String getLoginUser() {
		return loginUser;
	}

	public void setLoginUser(String loginUser) {
		this.loginUser = loginUser;
	}

	public String getOperateUser() {
		return operateUser;
	}

	public void setOperateUser(String operateUser) {
		this.operateUser = operateUser;
	}

	public Date getOperateTime() {
		return operateTime;
	}

	public void setOperateTime(Date operateTime) {
		this.operateTime = operateTime;
	}

	public String getActionSn() {
		return actionSn;
	}

	public void setActionSn(String actionSn) {
		this.actionSn = actionSn;
	}

	public String getOperateType() {
		return operateType;
	}

	public void setOperateType(String operateType) {
		this.operateType = operateType;
	}

	public String getOperateClass() {
		return operateClass;
	}

	public void setOperateClass(String operateClass) {
		this.operateClass = operateClass;
	}

	public Object getOperateObj() {
		return operateObj;
	}

	public void setOperateObj(Object operateObj) {
		this.operateObj = operateObj;
	}
}
