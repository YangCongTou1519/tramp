package com.tramp.basic.vo;

import java.util.List;

import com.tramp.basic.entity.OperateLogAction;

/**
 *  操作日志集合 Entity
 */
public class OperateLogVO {

	private Integer size;

	private OperateLogAction operateLogAction;

	private List<OperateLogDataVO> operateLogDataList;

	public Integer getSize() {
		return size;
	}

	public void setSize(Integer size) {
		this.size = size;
	}

	public List<OperateLogDataVO> getOperateLogDataList() {
		return operateLogDataList;
	}

	public void setOperateLogDataList(List<OperateLogDataVO> operateLogDataList) {
		this.operateLogDataList = operateLogDataList;
	}

	public OperateLogAction getOperateLogAction() {
		return operateLogAction;
	}

	public void setOperateLogActionEntity(OperateLogAction operateLogAction) {
		this.operateLogAction = operateLogAction;
	}
}
